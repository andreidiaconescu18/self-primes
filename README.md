# Self Primes

Self primes between A and B with variable number of threads(1 to 8) in C(pThreads), Java, Prolog and OpenMP.

The compilation and run in repo form of the programs will display only the execution time and the number of threads used to find self primes between A and B. To display self primes uncomment instruction for printing.

To generate program execution statistics we used scripts in bash. A main script launches another 4 sequential scripts, each for one of the requirements, and logs the result into files.

        Limits for A and B:
        pThreads, Java and OpenMP: A=2 and B=400000  
        Prolog: A=2 and B=10000

For execution the script you have to install [gcc](https://gcc.gnu.org/), [Java](https://www.oracle.com/technetwork/java/javaee/downloads/jdk8-downloads-2133151.html), [SWI-Prolog](http://www.swi-prolog.org/build/unix.html) and then grant permission to execute to main script:

        chmod +x self_number_script

Next step is to run the script.
        
        ./self_number_script

Project by Andrei Diaconescu    
