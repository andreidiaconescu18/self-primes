import java.util.ArrayList;
import java.util.List;

public class Main {
    public static int numberOfThreads;
    public static void main(String[] args) {
        long a = Integer.parseInt(args[0]);
        long b = Integer.parseInt(args[1]);
	    numberOfThreads = Integer.parseInt(args[2]);
        if (a%2==0)
            a++;
        List<Thread> list = new ArrayList<>();
        long ts = System.currentTimeMillis();
        for(int i=0; i<numberOfThreads; i++) {
            Thread t = new Thread(new MyClass(a+2*i, b, 2*numberOfThreads));
            list.add(t);
        }
        for(int i=0; i<numberOfThreads; i++){
            list.get(i).start();
        }
        try {
            for (int i = 0; i < numberOfThreads; i++) {
                list.get(i).join();
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        long tf= System.currentTimeMillis();
        System.out.println("Threads nr: "+numberOfThreads+" - Time: "+(tf-ts)/(double)1000);
    }

}
