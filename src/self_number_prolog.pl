:- set_prolog_flag(verbose, silent).
:- initialization(main).

divisible(X,Y) :- 0 is X mod Y, !.
divisible(X,Y) :- X > Y+1, divisible(X, Y+1).

isPrime(2) :- true,!.
isPrime(X) :- X < 2,!,false.
isPrime(X) :- not(divisible(X, 2)).

dd(0,C,C):-!.
dd(N,C,R):- N1 is div(N,10),C1 is C+1,dd(N1,C1,R).
d(N,R):-dd(N,0,R).

sodd(0,S,S):-!.
sodd(N,S,R):- C is mod(N,10), S1 is S+C, N1 is div(N,10), sodd(N1,S1,R).
sod(N,R):-sodd(N,0,R).

r(N,R):- N1 is N-1, C is mod(N1,9), R is C+1.

h(N,H):- r(N,R), C is mod(R,2), C==0,!, H is div(R,2).
h(N,H):- r(N,R), R9 is R+9, H is div(R9,2).

check_interval(_,_,-1):- true,!.
check_interval(N,H,K):- SS is N-H-9*K, SSabs is abs(SS), sod(SSabs,Ss), Sd is H+9*K, Ss \== Sd,!, K1 is K-1,check_interval(N,H,K1).

self_number(N):-d(N,D),h(N,H),check_interval(N,H,D).

worker(_,A,B,_):- A>B.
%worker(I,A,B,NT):- self_number(A),isPrime(A),!,format('~w ~w\n',[I,A]), AT is A+NT, AT=<B, worker(I,AT,B,NT).
worker(I,A,B,NT):- self_number(A),isPrime(A),!, AT is A+NT, AT=<B, worker(I,AT,B,NT).
worker(I,A,B,NT):- AT is A+NT,AT=<B,worker(I,AT,B,NT).

fa_thread(_,_,_,0,[]).
fa_thread(A,B,NT,N,[Id|R]):- thread_create(worker(A,A,B,NT),Id, []),N1 is N-1, A2 is A+2, fa_thread(A2,B,NT,N1,R).

asteapta_thread([]).
asteapta_thread([H|T]):- thread_join(H,_),asteapta_thread(T).

ex(A,B,N):- R is mod(A,2), R==0,!, A1 is A+1,NT is 2*N, fa_thread(A1,B,NT,N,L),asteapta_thread(L).
ex(A,B,N):- NT is 2*N,fa_thread(A,B,NT,N,L),asteapta_thread(L).

afiseaza_timp(A,B,N):-get_time(T1),ex(A,B,N),get_time(T2),Delta is T2-T1,format('Threads nr: ~w - Time: ~w\n',[N,Delta]).

main :-	current_prolog_flag(argv, [As,Bs,Ns]),atom_number(As,A),atom_number(Bs,B),atom_number(Ns,N),afiseaza_timp(A,B,N),halt.
	
