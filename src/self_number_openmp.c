#include <omp.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#define BILLION 1E9;

#define NR 8

int NO_OF_THREADS = 0;

long d(long long n)
{
	long long sum = 0;
	while (n != 0)
	{
		sum += 1;
		n = n / 10;
	}
	return sum;
}

long r(long long n)
{
	return 1 + ((n - 1) % 9);
}

long long h(long long n)
{
	long rr = r(n);
	if (rr % 2 == 0)
		return rr / 2;
	else
	{
		return (rr + 9) / 2;
	}

}

long long sod(long long n)
{
	long long sum = 0;
	while (n != 0)
	{
		sum += n % 10;
		n = n / 10;
	}
	return sum;
}

int prime_number(long long n)
{
	for (long long i = 2; i <= n / 2; i++)
		if (n%i == 0)
			return 0;
	return 1;
}

int main(int argc, char *argv[])
{
	long long i,s;
	long long a = atoi(argv[1]);
	long long b = atoi(argv[2]);
	NO_OF_THREADS = atoi(argv[3]);
	if (a % 2 == 0)
		a++;
	int vec[NR];
	for (i = 0; i < NO_OF_THREADS; i++)
	{
		vec[i] = a;
		a += 2;
	}
 	struct timespec start,stop;
 	double accum;
    	clock_gettime(CLOCK_REALTIME,&start);
	omp_set_num_threads(NO_OF_THREADS);
	#pragma omp parallel private (i,s)
	{
		int id;
		id = omp_get_thread_num();
		s = vec[id];
		for(i=s; i<=b; i += 2 * NO_OF_THREADS)
		{
			long long dn = d(i);
			long long hn = h(i);
			int sem = 1;
			for (long long k = 0; k <= dn && sem; k++)
			{
				long long ss = sod(abs(i - hn - 9 * k));
				long long sd = hn + 9 * k;
				if (ss == sd)
					sem = 0;
			}
			if (sem && prime_number(i))
				;//printf("%lld %lld\n", s, i);

		}
	}
	clock_gettime(CLOCK_REALTIME,&stop);
    	accum = stop.tv_sec-start.tv_sec + (stop.tv_nsec-start.tv_nsec)/BILLION;
    	printf("Threads nr: %d - Time: %lf\n",NO_OF_THREADS,accum);
	return 0;
}
