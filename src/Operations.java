public class Operations {
    public static long d(long n)
    {
        long sum=0;
        while(n!=0)
        {
            sum+=1;
            n=n/10;
        }
        return sum;
    }

    public static long r(long n)
    {
        return 1+((n-1)%9);
    }

    public static long h(long n)
    {
        long rr = r(n);
        if(rr%2==0)
            return rr/2;
        else
        {
            return (rr+9)/2;
        }

    }

    public static long sod(long n)
    {
        long sum=0;
        while(n!=0)
        {
            sum+=n%10;
            n=n/10;
        }
        return sum;
    }

    public static boolean isPrimeNumber(long n)
    {
        for(long i=2; i<=n/2; i++)
        if(n%i==0)
            return false;
        return true;
    }



}
