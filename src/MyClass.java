import static java.lang.Math.abs;

public class MyClass implements Runnable{
    private long a;
    private long b;
    private int numberOfThreads;

    public MyClass(long a, long b, int numberOfThreads)
    {
        this.a=a;
        this.b=b;
        this.numberOfThreads=numberOfThreads;
    }

    public void run(){
        for(long i=a; i<=b; i+=numberOfThreads)
        {
            long dn = Operations.d(i);
            long hn = Operations.h(i);
            boolean sem=true;
            for(long k=0; k<=dn && sem; k++)
            {
                long ss= Operations.sod(abs(i-hn-9*k));
                long sd=hn+9*k;
                if(ss==sd)
                    sem=false;
            }
            if(sem && Operations.isPrimeNumber(i))
                ;//System.out.println(a+" "+i);
        }

    }
}
